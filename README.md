# Packer
## Assignment
You want to send your friend a package with different things.
Each thing you put inside the package has such parameters as index number, weight and cost. The
package has a weight limit. Your goal is to determine which things to put into the package so that the
total weight is less than or equal to the package limit and the total cost is as large as possible.
You would prefer to send a package which weights less in case there is more than one package with the
same price.
#### Input sample
Your program should accept as its first argument a path to a filename. The input file contains several
lines. Each line is one test case.
Each line contains the weight that the package can take (before the colon) and the list of things you
need to choose. Each thing is enclosed in parentheses where the 1 st number is a thing's index number,
the 2 nd is its weight and the 3 rd is its cost. E.g.<br />
81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48) <br />
8 : (1,15.3,€34) <br />
75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78) <br />
56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64) <br />
#### Output sample
For each set of things that you put into the package provide a list (items’ index numbers are separated
by comma). E.g.<br />
4 <br />
-<br />
2,7<br />
8,9
## Algoritm
My packer recursive solution has complexity O(2^N). Before recursive calculation I excluding all items which contain bigger than maximum weight of the current items set. It is possible to improve complexity to O(N*2^(N/2)) using "Meet in the middle" algorithm.
## Important classes
* Packer - entry point of application
* PackerCore - core class, implementing observer patten, provides concurrent processing 
* FileReader - service for reading files
* PackerFileParser - simple business object parser from input string
* DataSet - class for calculating most valuable valid combination of the items from one input file row.
## Comments
"Float data type should never be used for precise values, such as currency" - quote from the oracle docs. In my solution I using int values of "grams" and "cents" for providing operations with items.
