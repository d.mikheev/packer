package com.mobiquityinc.exception;

/**
 * Custom enum error messages
 */
public enum ErrorMessage {

    FILE_NOT_FOUND("File %s is not exist"),
    UNKNOWN_ERROR("Unknown error"),
    INVALID_PACKAGE_WEIGHT("Invalid package weight in row %s"),
    INVALID_ITEM_FORMAT("Invalid item format in %s in row %s"),
    INVALID_ITEMS_QUANTITY("Invalid items quantity %s in row %s"),
    INTERRUPTED_EXCEPTION("Interrupted exception");

    private final String errorTextTemplate;

    ErrorMessage(final String errorTextTemplate) {
        this.errorTextTemplate = errorTextTemplate;
    }

    @Override
    public String toString() {
        return errorTextTemplate;
    }

}
