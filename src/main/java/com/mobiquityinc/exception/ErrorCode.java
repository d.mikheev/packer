package com.mobiquityinc.exception;

/**
 * ErrorCode for localisation messages
 */
public enum ErrorCode {
    FILE_NOT_FOUND(1),
    UNKNOWN_ERROR(2),
    INVALID_PACKAGE_WEIGHT(3),
    INVALID_ITEM_FORMAT(4),
    INVALID_ITEMS_QUANTITY(5),
    INTERRUPTED_EXCEPTION(6);

    private final int code;

    ErrorCode(final int code){
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
