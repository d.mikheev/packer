package com.mobiquityinc.exception;

/**
 * The APIException wraps all unchecked standard Java exception and enriches them with a custom error code.
 * You can use this code to retrieve localized error messages.
 */
public class APIException extends Exception {

    private final ErrorCode code;

    public APIException(ErrorMessage errorMessage, String[] args, ErrorCode code, Throwable cause) {
        super(String.format(errorMessage.toString(),args),cause);
        this.code = code;
    }

    public APIException(ErrorMessage errorMessage, String[] args, ErrorCode code) {
        super(String.format(errorMessage.toString(),args));
        this.code = code;
    }

    public APIException(ErrorMessage errorMessage, ErrorCode code, Throwable cause) {
        super(String.format(errorMessage.toString(),cause));
        this.code = code;
    }
}
