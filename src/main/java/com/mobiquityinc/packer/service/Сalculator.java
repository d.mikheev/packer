package com.mobiquityinc.packer.service;

import com.mobiquityinc.packer.model.DataSet;
import com.mobiquityinc.packer.model.DataSetComb;
import com.mobiquityinc.packer.model.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class Сalculator implements Callable {

    private final DataSet dataSet;
    List<DataSetComb> allCombinations = new ArrayList<>();

    public Сalculator(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    /**
     * Recursion search all valid items combinations and save it to allCombinations list
     * @param comb previous combination of items
     * @param iterator item index (item position in the input file row)
     */
    private void calcAllItemsCombs(DataSetComb comb, int iterator, List<Item> items){
        if (iterator==items.size()){ // exit from recursion
            allCombinations.add(comb);
            return;
        }
        Item nextItem = items.get(iterator);
        if ((nextItem.getWeight()+comb.getWeight())>dataSet.getMaxWeight()) { // overload weight condition when we take new item
            calcAllItemsCombs(new DataSetComb(comb),iterator+1,items);
        }
        else{

            calcAllItemsCombs( new DataSetComb(nextItem.getIndex(),  //  add next item to comb
                    comb.getWeight()+nextItem.getWeight(),comb.getPrice()+nextItem.getPrice(),comb.getElements()),iterator+1,items);

            calcAllItemsCombs(new DataSetComb(comb),iterator+1,items); // do not add next item to comb
        }
    }

    /**
     * Exclude trivial items from dataSet
     * @return items without trivial elements
     */
    private List<Item> excludeTrivialItemsHeuristic() {
        return dataSet.getItems().stream()
                .filter(e->e.getWeight()<=dataSet.getMaxWeight())
                .collect(Collectors.toList());
    }

    /**
     *
     * @return best combination from this DataSet
     */
    public String selectPackageBestItems() {
        List<Item> itemsWithoutTrivial = excludeTrivialItemsHeuristic();
        calcAllItemsCombs(new DataSetComb(),0,itemsWithoutTrivial);
        Collections.sort(allCombinations);
        return allCombinations.get(0).getElements();
    }

    @Override
    public String call() {
        return selectPackageBestItems();
    }
}
