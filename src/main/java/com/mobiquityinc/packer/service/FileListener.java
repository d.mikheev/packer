package com.mobiquityinc.packer.service;

import com.mobiquityinc.exception.APIException;

public interface FileListener {
    void onNextRow(String string) throws APIException;
    void fileFinished() throws APIException;
}
