package com.mobiquityinc.packer.service;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.exception.ErrorCode;
import com.mobiquityinc.exception.ErrorMessage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReader {

    private List<FileListener> listeners;

    public void addListener(FileListener listener){
        listeners.add(listener);
    }

    private void notifyOnNextRow(String row) throws APIException{
        for(FileListener listener:listeners){
            listener.onNextRow(row);
        }
    }

    private void notifyOnFileFinished() throws APIException {
        for(FileListener listener:listeners){
            listener.fileFinished();
        };
    }

    public FileReader() {
        listeners = new ArrayList<>();
    }

    public void readFromFile(String filePath) throws APIException {
        FileInputStream inputStream = null;
        Scanner sc = null;
        try {
            inputStream = new FileInputStream(filePath);
            sc = new Scanner(inputStream, "UTF-8");
            while (sc.hasNextLine()) {
                String row = sc.nextLine();
                notifyOnNextRow(row);
            }
            notifyOnFileFinished();
        }
        catch (FileNotFoundException e){
            throw new APIException(ErrorMessage.FILE_NOT_FOUND, new String[]{filePath}, ErrorCode.FILE_NOT_FOUND);
        }
        catch (IOException e) {
            throw new APIException(ErrorMessage.UNKNOWN_ERROR, ErrorCode.UNKNOWN_ERROR, e);
        }
        finally {
            closeScannerAndInputSteam(inputStream,sc);
        }
    }

    private static void closeScannerAndInputSteam(FileInputStream inputStream, Scanner sc) throws APIException{
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                throw new APIException(ErrorMessage.UNKNOWN_ERROR, ErrorCode.UNKNOWN_ERROR, e);
            }
        }
        if (sc != null) {
            sc.close();
        }
    }
}
