package com.mobiquityinc.packer.service;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.exception.ErrorCode;
import com.mobiquityinc.exception.ErrorMessage;
import com.mobiquityinc.packer.model.DataSet;
import com.mobiquityinc.packer.service.parser.DataParser;
import com.mobiquityinc.packer.service.parser.PackerFileParser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class PackerСore implements FileListener {

    private DataParser parser;
    private String packedOverallString;
    private ExecutorService cpuBound;
    private List<Callable<String>> calculateTasks = new ArrayList<>();
    private AtomicInteger threadCounter;
    private static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();


    public PackerСore(){
        this.parser = new PackerFileParser();
        this.threadCounter = new AtomicInteger(0);
        this.parser = new PackerFileParser();
        this.cpuBound = Executors.newFixedThreadPool(AVAILABLE_PROCESSORS);
        this.packedOverallString = "";
    }

    public String packFile(String filePath) throws APIException {
        FileReader fileReader = new FileReader();
        fileReader.addListener(this);
        fileReader.readFromFile(filePath);
        return packedOverallString;
    }

    @Override
    /**
     * Process file row
     */
    public void onNextRow(String row) throws APIException {
        if (threadCounter.get()<AVAILABLE_PROCESSORS) {
            DataSet dataSet = parser.getDataSet(row);
            calculateTasks.add(new Сalculator(dataSet));
            threadCounter.getAndIncrement();
            return;
        }
        processAllCalculateTasks();
    }

    /**
     * Parallely calculate all callable in calculateTasks
     * @throws APIException
     */
    private void processAllCalculateTasks() throws APIException{
        try {
            List<Future<String>> results = cpuBound.invokeAll(calculateTasks);
            for(Future<String> result:results){
                packedOverallString=packedOverallString + result.get()+"\n";
            }
            calculateTasks.clear();
            threadCounter.getAndSet(0);
        } catch (InterruptedException | ExecutionException e) {
            throw new APIException(ErrorMessage.UNKNOWN_ERROR, ErrorCode.UNKNOWN_ERROR, e);
        }
    }

    @Override
    public void fileFinished() throws APIException{
        processAllCalculateTasks();
    }

}
