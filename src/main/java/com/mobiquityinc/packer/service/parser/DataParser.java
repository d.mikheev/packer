package com.mobiquityinc.packer.service.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.DataSet;
import com.mobiquityinc.packer.model.Item;

import java.util.List;

/**
 * Parser interface
 */
public interface DataParser {
        public DataSet getDataSet(String string) throws APIException;
        public int readWeight(String row) throws APIException;
        public List<Item> readItems(String row) throws APIException;
}