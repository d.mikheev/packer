package com.mobiquityinc.packer.service.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.exception.ErrorCode;
import com.mobiquityinc.exception.ErrorMessage;
import com.mobiquityinc.packer.model.DataSet;
import com.mobiquityinc.packer.model.Amount;
import com.mobiquityinc.packer.model.Currency;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.service.parser.DataParser;

import java.util.ArrayList;
import java.util.List;

/*
Simple business object parser from input String
 */
public class PackerFileParser implements DataParser {

    private final static String WEIGHT_AND_COLUMN_REGEX = "^(\\d\\d?\\d?\\s:\\s).*";
    private final static String ITEM_PATTERN = "\\(\\d\\d?\\,[0-9]*\\.?[0-9]*\\,\\€[0-9]*\\.?[0-9]*\\)";
    private final static int MAX_WORDS_QUANTITY_IN_ROW = 17;
    private final static int WORD_INDEX_WHEN_ITEMS_START = 2;
    private final static int MIN = 0;
    private final static int MAX = 100;
    private final static int MAX_INDEX = 100;

    /**
     * Parse valid items from the row
     * @param row input file row
     * @return valid items list
     * @throws APIException exception cover
     */
    public List<Item> readItems(String row) throws APIException {
        List<Item> items = new ArrayList<>();
        String[] words = row.split(" ");
        if (words.length>MAX_WORDS_QUANTITY_IN_ROW||words.length<=WORD_INDEX_WHEN_ITEMS_START)
            throw new APIException(ErrorMessage.INVALID_ITEMS_QUANTITY,
                    new String[]{String.valueOf(words.length-WORD_INDEX_WHEN_ITEMS_START),row}, ErrorCode.INVALID_ITEMS_QUANTITY);

        for(int i=WORD_INDEX_WHEN_ITEMS_START;i<words.length;i++){
            if (!words[i].matches(ITEM_PATTERN))
                throw new APIException(ErrorMessage.INVALID_ITEM_FORMAT,new String[]{words[i], row}, ErrorCode.INVALID_ITEM_FORMAT);
            Item item = validateItemValues(words[i],row);
            items.add(item);
        }
        return items;
    }

    /**
     * Validate item params
     * @param word current item
     * @param row current row
     * @return
     * @throws APIException exception cover
     */
    private Item validateItemValues(String word, String row) throws APIException{
        String wordWithoutBracers = word.substring(1,word.length()-1); // remove bracers
        String[] itemElements = wordWithoutBracers.split(",");
        int itemIndex = Integer.parseInt(itemElements[0]);
        double itemWeight = Double.parseDouble(itemElements[1]);
        int itemPrice = Integer.parseInt(itemElements[2].substring(1)); // remove €
        if (itemIndex>MIN&&itemIndex<=MAX_INDEX&&itemWeight>MIN&&itemWeight<=MAX&&itemPrice>MIN&&itemPrice<=MAX)
            return new Item(itemIndex,(int)(Math.round(itemWeight*1000)),new Amount(Currency.EUR,Math.round(itemPrice*100)));
        throw new APIException(ErrorMessage.INVALID_ITEM_FORMAT,new String[]{word, row}, ErrorCode.INVALID_ITEM_FORMAT);
    }


    /**
     * Parse valid total weight from the row
     * @param row input file row
     * @return weight [1..100]*1000
     * @throws APIException exception cover
     */
    public int readWeight(String row) throws APIException {
        if (row.matches(WEIGHT_AND_COLUMN_REGEX)){
            String[] words = row.split(" ");
            Integer weight = Integer.parseInt(words[0]);
            if (weight>MIN && weight<=MAX)
                return weight*1000;
        }
        throw new APIException(ErrorMessage.INVALID_PACKAGE_WEIGHT,new String[]{row}, ErrorCode.INVALID_PACKAGE_WEIGHT);
    }

    @Override
    public DataSet getDataSet(String row) throws APIException {
       List<Item> rowItems = readItems(row);
       int rowWeight = readWeight(row);
       return new DataSet(rowWeight,rowItems);
    }
}