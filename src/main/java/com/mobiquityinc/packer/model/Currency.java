package com.mobiquityinc.packer.model;

/**
 * Currency type for different currencies
 */
public enum Currency {
    EUR(978,"€");

    private final int code;
    private final String symbol;

    Currency(int code, String symbol){
        this.code = code;
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
