package com.mobiquityinc.packer.model;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO for containing all row artifacts
 */
public class DataSet {

    private final int maxWeight;
    private final List<Item> items;

    List<DataSetComb> allCombinations = new ArrayList<>();

    /**
     *
     * @param weight total possible weight for combinations
     * @param items items from one row of the input file
     */
    public DataSet(int weight, List<Item> items){
        this.maxWeight = weight;
        this.items = items;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public List<Item> getItems() {
        return items;
    }
}