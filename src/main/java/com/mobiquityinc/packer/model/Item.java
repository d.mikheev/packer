package com.mobiquityinc.packer.model;

import java.util.Objects;

/**
 * DTO for package item from the input file row
 */
public class Item {

    private int index;
    private int weight;
    private Amount price;

    /**
     *
     * @param index item index
     * @param weight item weight
     * @param price item price in cents
     */
    public Item(int index, int weight, Amount price) {
        this.index = index;
        this.weight = weight;
        this.price = price;
    }

    public int getIndex() {
        return index;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrice() {
        return price.getAmountInCents();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return getIndex() == item.getIndex() &&
                getWeight() == item.getWeight() &&
                Objects.equals(getPrice(), item.getPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIndex(), getWeight(), getPrice());
    }
}
