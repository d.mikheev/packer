package com.mobiquityinc.packer.model;

/**
 * DTO for containing currency in cents
 */
public class Amount {

    private final int amountInCents;
    private final Currency cur;

    public Amount(Currency cur, int amountInCents){
        this.amountInCents = amountInCents;
        this.cur = cur;
    }

    public int getAmountInCents() {
        return amountInCents;
    }
}
