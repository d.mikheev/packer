package com.mobiquityinc.packer.model;

import java.util.Comparator;
import java.util.Objects;

import static java.util.Comparator.comparing;

/**
 * Container for valid items combinations
 */
public class DataSetComb implements Comparable<DataSetComb> {

    private String elements;
    private int weight;
    private int price;

    private Comparator<DataSetComb> COMPARATOR = getComparator();

    public DataSetComb() {
        elements="-";
    }

    public DataSetComb(DataSetComb dataSetComb){
        this.elements = dataSetComb.getElements();
        this.weight = dataSetComb.getWeight();
        this.price = dataSetComb.getPrice();
    }

    /**
     *
     * @param id item id
     * @param weight item weight
     * @param price item price in cents
     * @param elements sequence of items ids separated by commas
     */
    public DataSetComb(int id, int weight, int price, String elements) {
        this.elements = addIdToElement(elements,id);
        this.weight = weight;
        this.price = price;
    }

    private String addIdToElement(String elements,int id) {
        if (elements.equals("-"))  //first element condition
           return String.valueOf(id);
        return elements + "," + String.valueOf(id);
    }

    public String getElements() {
        return elements;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataSetComb)) return false;
        DataSetComb that = (DataSetComb) o;
        return weight == that.weight &&
                price == that.price &&
                Objects.equals(elements, that.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements, weight, price);
    }

    @Override
    public int compareTo(DataSetComb that) {
        int result = COMPARATOR.compare(this, that);
        return result;
    }

    /**
     * Items comparing conditions
     * @return comparator
     */
    private static Comparator<DataSetComb> getComparator(){
        Comparator<DataSetComb> result =
                comparing(DataSetComb::getPrice).reversed()
                        .thenComparing(DataSetComb::getWeight);
        return result;
    }
}
