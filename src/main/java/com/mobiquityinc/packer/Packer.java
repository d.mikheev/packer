package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.service.PackerСore;

/**
 * Class for managing packing process
 */
public class Packer {

    /**
     * Entry point of the library
     * @param s input file path
     * @return
     * @throws APIException packer exception cover
     */
    public static String pack(String s) throws APIException {
        PackerСore packerCore = new PackerСore();
        return packerCore.packFile(s);
    }

}