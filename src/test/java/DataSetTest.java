import com.mobiquityinc.packer.model.DataSet;
import com.mobiquityinc.packer.model.Amount;
import com.mobiquityinc.packer.model.Currency;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.service.Сalculator;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DataSetTest {

    @Test
    public final void calculateCustomRandomDataSet() {
        List<Item> items = new ArrayList(){{
            add(new Item(1,2,new Amount(Currency.EUR,15)));
            add(new Item(2,3,new Amount(Currency.EUR,2)));
            add(new Item(3,4,new Amount(Currency.EUR,7)));
            add(new Item(4,5,new Amount(Currency.EUR,23)));
            add(new Item(5,6,new Amount(Currency.EUR,7)));
            add(new Item(6,7,new Amount(Currency.EUR,18)));
        }};
        DataSet dataSet = new DataSet(12,items);
        Assert.assertTrue(new Сalculator(dataSet).call().equals("1,3,4"));
    }

    @Test
    public final void calculateCustomDataSetWithDublicateWeights(){
        List<Item> items = new ArrayList(){{
            add(new Item(1,2,new Amount(Currency.EUR,15)));
            add(new Item(2,3,new Amount(Currency.EUR,2)));
            add(new Item(3,2,new Amount(Currency.EUR,7)));
            add(new Item(4,6,new Amount(Currency.EUR,23)));
            add(new Item(5,2,new Amount(Currency.EUR,16)));
            add(new Item(6,7,new Amount(Currency.EUR,18)));
        }};
        DataSet dataSet = new DataSet(12,items);
        Assert.assertTrue(new Сalculator(dataSet).call().equals("1,3,4,5"));
    }

    @Test
    public final void calculateCustomDataSetWithRandowItemIndexes(){
        List<Item> items = new ArrayList(){{
            add(new Item(37,2,new Amount(Currency.EUR,15)));
            add(new Item(69,3,new Amount(Currency.EUR,2)));
            add(new Item(3,2,new Amount(Currency.EUR,7)));
            add(new Item(88,6,new Amount(Currency.EUR,23)));
            add(new Item(99,2,new Amount(Currency.EUR,16)));
            add(new Item(59,7,new Amount(Currency.EUR,18)));
        }};
        DataSet dataSet = new DataSet(12,items);
        Assert.assertTrue(new Сalculator(dataSet).call().equals("37,3,88,99"));
    }
}