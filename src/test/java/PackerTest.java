import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.Packer;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class PackerTest {

    @Test(expected = APIException.class)
    public final void whenInputFileIsNotExistThenAPIExceptionIsThrown() throws APIException {
        String result = Packer.pack("un_existing_file_name.txt");
    }

    @Test
    public final void functionalTestFromExampleInAssignment() throws APIException {
        String userDir = System.getProperty("user.dir");
        Assert.assertEquals(Packer.pack(userDir+"/src/test/resources/test_file_01.txt"),
                "4\n" +
                        "-\n" +
                        "2,7\n" +
                        "8,9\n");
    }

}