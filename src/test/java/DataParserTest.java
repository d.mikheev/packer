import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.Amount;
import com.mobiquityinc.packer.model.Currency;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.service.parser.DataParser;
import com.mobiquityinc.packer.service.parser.PackerFileParser;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class DataParserTest {

    @Test(expected = APIException.class)
    public final void whenInvalidWeightInRowAPIExceptionThrow_01() throws APIException {
        DataParser dataParser = new PackerFileParser();
        dataParser.getDataSet("0 : ");
    }

    @Test
    public final void calculateValidWeightInRow() throws APIException {
        DataParser dataParser = new PackerFileParser();
        Assert.assertEquals(dataParser.readWeight("1 : (1,85.31,€29) (2,14.55,€74)"),1000); //weight in grams
        Assert.assertEquals(dataParser.readWeight("100 : (1,85.31,€29) (2,14.55,€74)"),100000);  //weight in grams
        Assert.assertEquals(dataParser.readWeight("58 : (1,85.31,€29) (2,14.55,€74)"),58000);  //weight in grams
    }

    @Test(expected = APIException.class)
    public final void whenInvalidWeightInRowAPIExceptionThrow_02() throws APIException {
        DataParser dataParser = new PackerFileParser();
        Assert.assertEquals(dataParser.readWeight("10000 : (1,85.31,€29) (2,14.55,€74)"),10000);
    }

    @Test
    public final void calculateValidItemsInRow() throws APIException {
        DataParser dataParser = new PackerFileParser();
        Assert.assertTrue(dataParser.readItems("81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9)")
                .equals(new ArrayList<Item>(){{add(new Item(1,53380,new Amount(Currency.EUR,4500)));
                    add(new Item(2,88620,new Amount(Currency.EUR,9800)));
                    add(new Item(3,78480,new Amount(Currency.EUR,300)));
                    add(new Item(4,72300,new Amount(Currency.EUR,7600)));
                    add(new Item(5,30180,new Amount(Currency.EUR,900)));
                }}));

        dataParser.readItems("75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) " +
                "(6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)");

        dataParser.readItems("56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) " +
                "(6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)");
    }


}